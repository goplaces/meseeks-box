defmodule Bus do
  def get_ibibo_details(bus) do
    temp_map = Map.new()

    result =
      Map.merge(temp_map, %{
        fare: String.to_integer("#{bus["fare"]["totalfare"]}"),
        rating: "#{bus["rating"]}",
        busType: "#{bus["BusType"]}",
        arrivalTime: "#{bus["ArrivalTime"]}",
        departureTime: "#{bus["DepartureTime"]}",
        avlWindowSeats: "#{bus["avlWindowSeats"]}",
        avlSeats: nil,
        busCompany: "#{bus["busCompany"]}",
        travelsName: "#{bus["TravelsName"]}",
        site: "goibibo"
      })
  end

  def get_apsrtc_details(bus, bus_no) do
    temp_map = Map.new()
    [srNO | _] = Floki.find(bus, "div.srvceNO")
    [srvceNO | _] = elem(srNO, 2)
    srvceNO = String.replace(srvceNO, ~r"[\n \t]", "")

    [strtm | _] = Floki.find(bus, "span#bPtsDiv_Forward#{bus_no}")
    [strtm | _] = elem(strtm, 2)

    [arrtm | _] = Floki.find(bus, "span#aPtsDiv_Forward#{bus_no}")
    [arrtm | _] = elem(arrtm, 2)

    [fare | _] = Floki.find(bus, ".rupeeIco")
    [fare | _] = elem(fare, 2)
    fare = String.to_integer(String.replace(fare, ~r"[\n \t ,]", ""))

    [avlseats | _] = Floki.find(bus, ".availCs")
    [avlseats | _] = elem(avlseats, 2)
    avlseats = String.replace(avlseats, ~r"[\n \t]", "")

    [busType | _] = Floki.find(bus, "div.col3-left")
    [busType | _] = elem(busType, 2)
    [busType | _] = elem(busType, 2)
    busType = String.replace(busType, ~r"[\n \t ,]", "")

    result =
      Map.merge(temp_map, %{
        fare: fare,
        arrivalTime: arrtm,
        departureTime: strtm,
        travelsName: srvceNO,
        avlSeats: avlseats,
        avlWindowSeats: nil,
        rating: nil,
        busType: busType,
        busCompany: nil,
        site: "apsrtc"
      })
  end
end
