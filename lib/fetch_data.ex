defmodule FetchData do
  def fetch_sites_data(sites, from, to, date) do
    IO.puts("Search for buses between #{from} and #{to} on #{date} ...")

    busdata =
      Enum.map(sites, fn site ->
        if site["enabled"] do
          api_data = Site.make_request(site["data"]["request"], from, to, date)
          Site.get_buses(api_data, site["data"]["request"]["format"])
        end
      end)

    [_ | total_buses] = busdata
    List.flatten(total_buses)
  end
end
