defmodule Meseeks do
  def search(from, to, date) do
    datetime = Timex.parse!(date, "%d-%b-%Y", :strftime)

    {:ok, datetime_string} = Timex.format(datetime, "{D}-{M}-{YYYY}")
    IO.puts("Search for buses between #{from} and #{to} on #{datetime_string} ...")

    source_path = Path.join([File.cwd!(), "lib", "data", "sources.yml"])
    data_sources = Initializer.yaml_parser(source_path)
    filters = %{from: from, to: to, date: datetime}

    data_sources
    |> Enum.filter(fn site -> Map.get(site, "enabled") end)
    |> Enum.map(fn site -> fetch_from_source(site, filters) end)

    # |> format
  end

  def fetch_from_source(site, filters) do
    IO.puts("Fetching from #{Map.get(site, "name")}")

    request_details =
      site
      |> Map.get("data")
      |> Map.get("request")

    IO.inspect(request_details)

    Source.fetch(request_details, filters)

    # Site.get_buses(api_data, site["data"]["request"]["format"])
  end

  def parse_and_normalize(response, site) do
    IO.puts("Parsing response from - #{Map.get(site, "name")}")

    response_details =
      site
      |> Map.get("data")
      |> Map.get("response")

    Response.parse(response_details, response)
  end
end
