use Timex

defmodule Helper do
  def formatter(date, format) do
    Timex.format(date, format)
  end

  def get_url(request, from, to, date) do
    IO.inspect("Initial Date is #{date}")

    if request["format"] == "htmlParse" do
      date = elem(Helper.formatter(date, request["date_format"]), 1)
      date = String.replace(date, "-", "/")
      from = String.upcase(from)
      to = String.upcase(to)
      IO.inspect("Apsrtc date is printing here")
      IO.inspect(date)

      "#{request["url"]}?txtJourneyDate=#{date}&startPlaceId=#{
        ApsrtcCityid.get_city_id("#{from}")
      }&endPlaceId=#{ApsrtcCityid.get_city_id("#{to}")}"
    else
      date = elem(Helper.formatter(date, request["date_format"]), 1)
      IO.inspect("Gobibo date is printing here")
      IO.inspect(date)

      params =
        Map.merge(request["params"], %{
          source: from,
          destination: to,
          dateofdeparture: date |> String.split("-") |> Enum.join()
        })

      "#{request["url"]}?#{get_params_string(params)}"
    end
  end

  def get_params_string(params) do
    out = URI.encode_query(params)
  end
end
