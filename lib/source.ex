defmodule Source do
  def fetch(request, filters) do
    response =
      case Map.get(request, "type") do
        "http" ->
          HttpClient.make_request(
            Map.get(request, "url"),
            Map.get(request, "action"),
            Map.get(request, "headers"),
            Map.get(request, "params"),
            filters
          )
      end

    # else
    #   case HTTPoison.get(url, options) do
    #     {:ok,
    #      %HTTPoison.Response{
    #        body: body,
    #        headers: headers,
    #        request: request,
    #        request_url: request_url,
    #        status_code: 200
    #      }} ->
    #       out =
    #         body
    #         |> Floki.find("div.rSetForward")

    #     _ ->
    #       IO.puts("nothing ")
    #   end
    # end
  end

  def get_buses(api_data, format) do
    if format == "apiRequest" do
      api_data = elem(api_data, 1)
      buses = api_data["data"]["onwardflights"]

      bus_data =
        Enum.map(buses, fn bus ->
          output = Bus.get_ibibo_details(bus)
          IO.inspect(output)
        end)
    else
      api_data
      |> Enum.with_index(0)
      |> Enum.map(fn {bus, bus_no} ->
        outp = Bus.get_apsrtc_details(bus, bus_no)
        IO.inspect(outp)
      end)
    end
  end
end
