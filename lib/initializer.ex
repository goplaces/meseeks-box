defmodule Initializer do
  def yaml_parser(yaml_path) do
    # The yaml_elixir application must be started before any use of it.
    Application.ensure_all_started(:yaml_elixir)
    sites_info = elem(YamlElixir.read_from_file(yaml_path), 1)["websites"]
  end
end
