use Timex

# Call the module with HttpClient.make_request/5

defmodule HttpClient do
  def make_request(url, action, headers, params, filters) do
    case action do
      "get" ->
        params_to_attach =
          params
          |> Enum.map(fn param -> populate(param, filters) end)
          |> Map.new()

        url = make_url(url, params_to_attach)

        IO.inspect(url)

        headers_to_attach =
          headers
          |> Enum.map(fn header -> populate(header, filters) end)
          |> Enum.filter(&(!is_nil(&1)))
          |> Map.new()

        IO.puts(inspect(headers_to_attach))
        options = [recv_timeout: 20_000]

        case HTTPoison.get(url, headers_to_attach, options) do
          {:ok, %HTTPoison.Response{status_code: 200, body: body}} -> body
          {:ok, %HTTPoison.Response{status_code: 401}} -> IO.puts("Unauthorised")
        end
    end
  end

  def make_url(url, params) do
    "#{url}?#{URI.encode_query(params)}"
  end

  def resolve_value(key, pairs, default_value) do
    if key do
      case Map.fetch(pairs, String.to_atom(key)) do
        {:ok, substitution_value} -> substitution_value
        :error -> IO.puts("Did not find #{key} in given #{inspect(pairs)}")
      end
    else
      default_value
    end
  end

  def populate(param, filters) do
    substitution_key = Map.get(param, "substitute")
    default_value = Map.get(param, "value")

    value = resolve_value(substitution_key, filters, default_value)

    case Map.get(param, "type") do
      "string" ->
        {Map.get(param, "key"), value}

      "date" ->
        date_string = Timex.format!(value, Map.get(param, "format"))
        {Map.get(param, "key"), date_string}

      nil ->
        nil
    end
  end
end
