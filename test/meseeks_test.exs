defmodule FetchDataTest do
  use ExUnit.Case
  import Mock

  test "mock" do
    with_mock Source, [fetch: fn _, _ -> response end] do

      result = Meseeks.fetch_from_source(site, filters)

      assert result == response
    end
  end

  defp site do
    %{
      "data" => %{
        "request" => %{
          "action" => "get",
          "headers" => [%{"key" => "Accept", "value" => "application/json; charset=utf-8"}],
          "params" => [
            %{"key" => "app_id", "type" => "string", "value" => "app_id_string"},
            %{"key" => "app_key", "type" => "string", "value" => "app_key_string"},
            %{"key" => "format", "type" => "string", "value" => "json"},
            %{"key" => "source", "substitute" => "from", "type" => "string"},
            %{"key" => "destination", "substitute" => "to", "type" => "string"},
            %{"format" => "{YYYY}{M}{D}", "key" => "dateofdeparture", "substitute" => "date", "type" => "date"}
          ],
          "type" => "http",
          "url" => "http://developer.goibibo.com/api/bus/search/"
        },
      },
      "enabled" => true,
      "name" => "ibibo"
    }
  end

  defp filters do
    %{from: "Bangalore", to: "Hyderabad", date: ~N[2020-04-20 00:00:00]}
  end

  defp response do
    %{
      "data" => %{
        "onwardflights" => []
      }
    }
  end
end

