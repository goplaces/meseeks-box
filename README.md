This is the engine that backs GoPlaces. This engine is responsible for the following :
* Store list of backends (sources to be searched)
* Fetch filtered results from those backends
* Sort and filter based on the given preferences
* Format them into a common object format for clients to consume

#### Local Development

``` shell
mix deps.get
iex -S mix
> Meseeks.search("Bangalore", "Hyderabad", "20-Apr-2020")
```

#### Why the name - Meseeks Box ?
The Mr. Meeseeks Box is a gadget that creates a Mr. Meeseeks for the purpose of completing one given objective, with one simple button.

![](docs/mr-meseeks-box.png)

Now press that button !
